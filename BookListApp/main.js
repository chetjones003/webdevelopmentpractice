// -------------------------- BOOK CLASS -------------------------------- //
class Book 
{
    constructor(title, author, isbn)
    {
        // setting the constructor values to be 
        // whatever tile/author/isbn is passed in
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    };
}


// -------------------------- UI CLASS ----------------------------------- //
class UI 
{
    // Don't want to instantiate ui elements so I'm using static
    // to hard code an array of books
    // basically serving as place holders until I added the add book functionality
    static displayBooks() 
    {
        const books = Store.getBooks();

        books.forEach((book) => UI.addBookToList(book));
    };

    static addBookToList(book)
    {
        // grabbing the book-list id from the tbody
        const list = document.querySelector('#book-list');

        // creating table row element
        const row = document.createElement('tr');
        row.innerHTML =
            `

            <td>${book.title}</td>
            <td>${book.author}</td>
            <td>${book.isbn}</td>
            <td><a href="#" class="btn btn-danger btn-sm delete">X</a></td>
        `;

        // append row to list
        list.appendChild(row);
    };

    static deleteBook(target)
    {
        // only want to do this if the delete button is clicked
        if(target.classList.contains('delete'))
        {
            target.parentElement.parentElement.remove();
        }
    };

    // building a custom alert
    static showAlert(message, className)
    {
        const div = document.createElement('div');
        // giving the alert a class
        div.className = `alert alert-${className}`;

        // adding the message
        div.appendChild(document.createTextNode(message));

        // where to put alert
        const container = document.querySelector('.container');

        // get the form
        const form = document.querySelector('#book-form');

        // insert alert
        container.insertBefore(div, form);

        // make alert vanish in 3 seconds
        setTimeout(() => document.querySelector('.alert').remove(), 3000);
    };

    static clearFields()
    {
        // grabbing the input values and clearing them
        document.querySelector('#title').value = '';
        document.querySelector('#author').value = '';
        document.querySelector('#isbn').value = '';
    };
}


// ---------------------- STORE CLASS ----------------------------------- //
class Store
{
    static getBooks()
    {
        let books;
        if(localStorage.getItem('books') === null)
        {
            books = [];
        }
        else
        {
            books = JSON.parse(localStorage.getItem('books'));
        }

        return books;
    };

    static addBook(book)
    {
        const books = Store.getBooks();

        books.push(book);

        localStorage.setItem('books', JSON.stringify(books));
    };

    static removeBook(isbn)
    {
        const books = Store.getBooks();

        books.forEach((book, index) =>
        {
            if (book.isbn === isbn)
            {
                books.splice(index, 1);
            }

            localStorage.setItem('books', JSON.stringify(books));
        });
    };
}


// ---------------------- EVENT: DISPLAY BOOKS -------------------------- //
document.addEventListener('DOMContentLoaded', UI.displayBooks);


// ---------------------- EVENT ADD BOOK -------------------------------- //
// grabbing the form
document.querySelector('#book-form').addEventListener('submit', (e) => 
{
    // prevent actual submit
    e.preventDefault();

    // get form values
    const title = document.querySelector('#title').value;
    const author = document.querySelector('#author').value;
    const isbn = document.querySelector('#isbn').value;

    // validation
    if (title === '' || author === '' || isbn === '')
    {
        UI.showAlert('Please fill in all fields', 'danger')
    }
    else
    {
        // instantiate book from book class
        const book = new Book(title, author, isbn);

        // add book to UI
        UI.addBookToList(book);

        // add book to storage
        Store.addBook(book);

        // show book add success message
        UI.showAlert('Book Successfully Created', 'success');

        // clear input fields
        UI.clearFields();
    }
});

// -------------------- EVENT: REMOVE BOOK  -------------------------------- //
// using event propagation to delete books
// this is to solve the problem of only delete the first item in the book list
// I am targeting the entire book list then checking if the clicked item has the 
// delete class and then deleting the book from list
document.querySelector('#book-list').addEventListener('click', (e) =>
{
    // remove book from ui
    UI.deleteBook(e.target);

    // show book delete success message
    UI.showAlert('Book Successfully Deleted', 'success');

    // remove book from storage
    Store.removeBook(e.target.parentElement.previousElementSibling.textContent);
});
