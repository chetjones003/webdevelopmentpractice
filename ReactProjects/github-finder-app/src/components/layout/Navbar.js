import React, { Component } from 'react'

export class Navbar extends Component {
  static defaultProps = {
    title: 'Github Finder',
    icon: 'fab fa-github-alt'
  }
  render() {
    return (
      <nav className='navbar bg-primary'>
       <h1>
         <i className={this.props.icon}></i>
         {this.props.title}
        </h1> 
      </nav>
    )
  }
}

export default Navbar